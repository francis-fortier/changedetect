import { Record } from 'immutable';

export interface OtherData {
    count: number;
}

const DEFAULTS: OtherData = {
    count: 0
};

export class OtherRecord extends Record<OtherData>(DEFAULTS) implements Readonly<OtherData> {
    constructor(data: OtherData) {
        super(data);
    }

    increment(): OtherRecord {
        return this.set('count', this.count + 1);
    }
}
