import { Injectable } from '@angular/core';
import { MyStore } from '../store.factory';
import { LOADING_THINGS, ItemLoadAction, THINGS_LOADED, PropertySetAction, SET_THING_PROPERTY } from '../store.actions';
import { ThingData, ThingStatus } from './things.model';
import { ThingsService } from '../../rest/things.service';
import { ThingModel } from '../../rest/things.model';
import { List as ImmutableList } from 'immutable';

@Injectable()
export class ThingsAction {
    constructor(
        private store: MyStore,
        private service: ThingsService,
    ) { }

    public generateThings(): void {
        this.store.dispatch({ type: LOADING_THINGS });

        const reducer = (items: { [id: string]: ThingData }, thing: ThingModel) => {
            return Object.assign(items, { [thing.id]: this.toThingData(thing) });
        };

        this.service.getThings()
            .map(things => things.reduce(reducer, {} as { [id: string]: ThingData }))
            .subscribe(items => {
                const action: ItemLoadAction<ThingData> = {
                    type: THINGS_LOADED,
                    items
                };

                this.store.dispatch(action);
            });
    }

    public changeName(id: string, value: string): void {
        const action: PropertySetAction<ThingData> = {
            type: SET_THING_PROPERTY,
            id,
            values: {
                name: value
            }
        };

        this.store.dispatch(action);
    }

    private toThingData(model: ThingModel): ThingData {
        return {
            status: ThingStatus[model.status],
            name: model.name,
            description: model.description,
            details: ImmutableList<string>(model.details),
        };
    }

}
