import { thingsReducer } from './things/things.reducer';
import { Store, Dispatch, Unsubscribe, AnyAction, createStore, combineReducers, StoreEnhancer } from 'redux';
import { Provider } from '@angular/core';
import { Map as ImmutableMap } from 'immutable';
import { ThingRecord } from './things/things.model';
import { Observable } from 'rxjs/Observable';
import { OtherRecord } from './other/other.model';
import { otherReducer } from './other/other.reducer';

declare var __REDUX_DEVTOOLS_EXTENSION__: <S>() => StoreEnhancer<S> | undefined;

export abstract class MyStore implements Store<StoreModel> {
    readonly changes: Observable<StoreModel>;
    dispatch: Dispatch<StoreModel>;
    abstract getState(): StoreModel;
    abstract subscribe(listener: () => void): Unsubscribe;
    abstract replaceReducer(nextReducer: (state: StoreModel, action: AnyAction) => StoreModel): void;
}

function storeMixIn(this: Store<StoreModel>): MyStore {
    return Object.defineProperties(this, {
        changes: {
            value: new Observable<StoreModel>(observer => {
                observer.next(this.getState());

                return this.subscribe(() => observer.next(this.getState()));
            }),
            writable: false
        }
    });
}

export interface StoreModel {
    things: ImmutableMap<string, ThingRecord>;
    other: OtherRecord;
}

export const STORE_PROVIDER: Provider = {
    provide: MyStore,
    useFactory: () => {
        const reducer = combineReducers<StoreModel>({
            things: thingsReducer,
            other: otherReducer,
        });

        const enhancer: StoreEnhancer<StoreModel> = typeof __REDUX_DEVTOOLS_EXTENSION__ === 'function'
            ? __REDUX_DEVTOOLS_EXTENSION__()
            : void (0);

        const store = createStore<StoreModel>(reducer, enhancer);

        return storeMixIn.call(store);
    }
};
