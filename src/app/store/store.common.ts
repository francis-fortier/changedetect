import { AnyAction, Reducer } from 'redux';
import * as Immutable from 'immutable';

const NOOP_REDUCER = <T>(state: T, action: AnyAction) => state;

export interface ReducersMap<T> {
    [key: string]: Reducer<T>;
}

export function buildReducer<T>(reducers: ReducersMap<T>, initialValue: T): Reducer<T> {
    return (state, action) => (reducers[action.type] || NOOP_REDUCER)(typeof state === 'undefined' ? initialValue : state, action);
}
