import { NgModule } from '@angular/core';
import { STORE_PROVIDER } from './store.factory';
import { ThingsAction } from './things/things.action';
import { RestModule } from '../rest/rest.module';
import { OtherAction } from './other/other.action';

@NgModule({
    imports: [
        RestModule,
    ],
    providers: [
        STORE_PROVIDER,
        ThingsAction,
        OtherAction,
    ]
})
export class StoreModule {

}
