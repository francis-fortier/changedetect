import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MyStore, StoreModel } from './store/store.factory';
import { ThingData, ThingStatus } from './store/things/things.model';
import { ThingsAction } from './store/things/things.action';
import { ArrayUtil } from './util/array-util';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
    selector: 'app-root',
    template: `
        <h1>Things! ({{count$ | async}} changes)</h1>
        <board [items]="undoneThings$ | async" category="To Do"></board>
        <board [items]="doingThings$ | async" category="Doing"></board>
        <board [items]="doneThings$ | async" category="Done"></board>
    `,
    styleUrls: ['./app.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnDestroy {
    public undoneThings$: Observable<[string, Readonly<ThingData>][]>;
    public doingThings$: Observable<[string, Readonly<ThingData>][]>;
    public doneThings$: Observable<[string, Readonly<ThingData>][]>;

    public count = 0;
    // public count$: Observable<number>;

    private subscription: Subscription;

    constructor(
        store: MyStore,
        thingsAction: ThingsAction,
        changeDetector: ChangeDetectorRef,
    ) {
        const things$ = store.changes
            .map(state => state.things)
            .distinctUntilChanged()
            .map(map => map.toArray())
            // toJS will create new references of everything so the change detector can't detect what really changed.
            // .map(map => map.toJS() as { [key: string]: Readonly<ThingData> })
            // .map(copy => Object.keys(copy).map(id => [id, copy[id]]))
            .publishReplay(1)
            .refCount();

        const statusFilter = (status: ThingStatus) => (upstream: Observable<[string, Readonly<ThingData>][]>) => upstream
            .scan((prev, things) => ArrayUtil.firstIfEqual(prev, things.filter(([, thing]) => thing.status === status)), [])
            .distinctUntilChanged();

        this.undoneThings$ = things$.let(statusFilter(ThingStatus.TODO));
        this.doingThings$ = things$.let(statusFilter(ThingStatus.DOING));
        this.doneThings$ = things$.let(statusFilter(ThingStatus.DONE));

        this.subscription = store.changes
            .map(state => state.other.count)
            .do(count => console.log(count))
            .subscribe(count => { this.count = count; changeDetector.markForCheck(); });

        // this.count$ = store.changes
        //     .map(state => state.other.count)
        //     .do(count => console.log(count));

        thingsAction.generateThings();
    }

    public ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

}
