import { Injectable } from '@angular/core';
import { ThingModel } from './things.model';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ThingsService {
    constructor(
        private http: HttpClient,
    ) { }

    getThings(): Observable<ThingModel[]> {
        return this.http.get<ThingModel[]>('/api/things');
    }
}
