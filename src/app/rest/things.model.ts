export interface ThingModel {
    id: string;
    name: string;
    description: string;
    status: 'TODO' | 'DOING' | 'DONE';
    details: string[];
}
