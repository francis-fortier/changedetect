import { Component, Input, ViewChildren, QueryList, AfterViewInit, ChangeDetectionStrategy, SimpleChanges } from '@angular/core';
import { ThingRecord, ThingData } from './store/things/things.model';
import { NgModel } from '@angular/forms';
import { ThingsAction } from './store/things/things.action';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
    selector: 'board',
    template: `
        <h2>{{category}}</h2>
        <ul>
            <li *ngFor="let item of items">
                <task [id]="item[0]" [item]="item[1]"></task>
            </li>
        </ul>
    `,
    // template: `
    //     <h2>{{category}}</h2>
    //     <ul>
    //         <li *ngFor="let item of items">
    //             <input #inputModel="ngModel" type="text"
    //                 [ngModel]="getName(item[1])" required
    //                 (blur)="changeName(item[0], inputModel)"/>
    //         </li>
    //     </ul>
    // `,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BoardComponent {
    @Input()
    public category: string;

    @Input()
    public items: [string, ThingRecord][];

    constructor(
        private action: ThingsAction,
    ) { }

    public changeName(id: string, inputModel: NgModel) {
        if (inputModel.valid && inputModel.dirty) {
            this.action.changeName(id, inputModel.value as string);
        }
    }

    public getName(item: Readonly<ThingData>) {
        console.log('getName', item.name);

        return item.name;
    }
}
