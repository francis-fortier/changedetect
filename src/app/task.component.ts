import { Component, Input, ChangeDetectionStrategy, NgZone, ChangeDetectorRef, OnChanges, OnInit } from '@angular/core';
import { ThingData } from './store/things/things.model';
import { NgModel } from '@angular/forms';
import { ThingsAction } from './store/things/things.action';
import { OtherAction } from './store/other/other.action';
import { MyStore } from './store/store.factory';
import { setInterval } from 'timers';

let first = true;

@Component({
    selector: 'task',
    template: `
        <input #inputModel="ngModel" type="text"
            [ngModel]="getName(item)" required
            (blur)="changeName(inputModel)"/>{{count}}
    `,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskComponent implements OnInit {
    @Input()
    public id: string;

    @Input()
    public item: Readonly<ThingData>;

    public count = 0;

    constructor(
        private action: ThingsAction,
        private otherAction: OtherAction,
        private store: MyStore,
        private zone: NgZone,
        private cd: ChangeDetectorRef,
    ) {
        if (first) {
            setInterval(() => {
                this.count++;
                cd.markForCheck();
            }, 500);
            first = false;
        }
    }

    public changeName(inputModel: NgModel): void {
        if (inputModel.valid && inputModel.dirty) {
            this.action.changeName(this.id, inputModel.value);
        }
    }

    public getName(item: Readonly<ThingData>) {
        console.log('getName', item.name);
        return item.name;
    }
    public ngOnInit(): void {
        this.otherAction.increment();
    }
}
