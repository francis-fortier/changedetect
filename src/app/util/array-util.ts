export const ArrayUtil = {
    // firstIfEqual<T>(first: T[], second: T[]): T[] {
    //     return second;
    // },
    firstIfEqual<K, V>(first: [K, V][], second: [K, V][]): [K, V][] {
        const results: [K, V][] = [];
        let allFound = first.length === second.length;

        for (let i = 0; i < second.length; i++) {
            let found: [K, V] = null;

            for (let j = 0; j < first.length && !found; j++) {
                if (second[i][0] === first[i][0] && second[i][1] === first[i][1]) {
                    found = first[i];
                } else {
                    allFound = false;
                }
            }

            results[i] = found || second[i];
        }

        return allFound ? first : results;
    }
};
